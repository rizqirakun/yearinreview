<!DOCTYPE html>
<html lang="id">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width: device-width, initial-scale=1.0">
	<title>{{ $shop }} - 2019 Year End Resume</title>

	<meta name="description" content="Ringkasan tahun 2019 toko {{ $shop }} di App Ngorder">
    <meta name="keywords" content="aplikasi penjualan, aplikasi toko online, software pos, software toko online gratis">

	<meta property="og:url"                content="https://2019.ngorder.id/{{ $shop }}" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="#2019YearEndResume - {{ $shop }}" />
    <meta property="og:description"        content="Banyak hal luar biasa di tahun 2019, mari melihat kembali pencapaian {{ $shop }} selama setahun ini." />
	<meta property="og:image"              content="https://2019.ngorder.id/images/2020-square.png" />
	<meta property="og:image:width"        content="600" />
	<meta property="og:image:height"       content="600" />

	<link rel="stylesheet" href="css/vendor.css">
	<link rel="stylesheet" href="css/app.css?v=2019.1">
</head>
<body>
	<div class="container p-0">
		<!-- header -->
		<header class="header">
			<div class="container bg-hero">
				<section class="hero">
					<div class="row hero-body">
						<div class="col-12 text-center">
							@if($logo)
								<img class="mb-3" src="{{ $logo }}" alt="{{ $shop }}" style="width: 100px;">
							@endif
							<h1 class="h2 hero-title font-weight-bold">
								<span>{{ $shop }}</span>
							</h1>
						</div>
						<div class="col-12 hero-desc text-center">
							<p class="lead text-light m-0">Banyak pencapaian hebat <strong>{{ $shop }}</strong> di tahun 2019!</p>
							<p class="text-light">Ngorder telah membuat ringkasannya untuk Anda.</p>
							<p class="text-light mt-4">#2019Resume</p>
						</div>	
					</div>
				</section>
			</div>
		</header>

		<!-- main -->
		<main class="main">
			<div class="container">
				<section class="info">
					<div class="row">

						<!-- Orderan -->
						<div class="col-12 col-md-8 mx-auto">
							<div class="card w-100 orderan wow fadeIn">
								<div class="card-body">
									<div class="info-text">
										<h3 class="info-text-title counter">{{ $order }}</h3>
										<h5 class="info-text-desc">Orderan</h5>
										<div class="info-text-badge">
											<?php if ($order_up > 0) {
												$upDownIcon = 'arrow-round-up';
												$upDownColor = 'success';
											} else {
												$upDownIcon = 'arrow-round-down';
												$upDownColor = 'danger';
											}?>
											<ion-icon name="{{ $upDownIcon}}" class="text-{{ $upDownColor}}"></ion-icon>
											<span class="text-{{ $upDownColor}}"><span>{{ $order_up_percent }}</span>%</span>
											<span class="text-muted">(<span class="counter">{{ $order_up }}</span> from last year)</span>
										</div>
									</div>
									<div class="info-content">
										<canvas class="orderan-chart"></canvas>
									</div>
								</div>
							</div>
						</div>

						<!-- Produk -->
						<div class="col-12 col-md-8 mx-auto">
							<div class="card w-100 produk wow fadeInUp">
								<div class="card-body">
									<div class="info-text mb-4 mb-md-5">
										<h3 class="info-text-title counter">{{ $product }}</h3>
										<h5 class="info-text-desc">Produk Terjual</h5>
										
										<div class="info-text-badge">
											<ion-icon name="{{ $upDownIcon}}" class="text-{{ $upDownColor}}"></ion-icon>
											<span class="text-{{ $upDownColor}}"><span>{{ $product_up_percent }}</span>%</span>
											<span class="text-muted">(<span class="counter">{{ $product_up }}</span> from last year)</span>
										</div>
									</div>
									<div class="info-content mx-auto">
										<div class="info-content-title vertical-text">
											<span>Popular</span>
										</div>
										<div class="produk-wrap">
											<div class="row">
												@if(isset($product_a_img))
												<div class="col col-12 col-sm-6 col-md-4">
													<div class="card produk-item">
														<div class="card-img-top" style="background-image: url('{{ $product_a_img }}')">
														</div>
														<div class="card-body">
															<h5 class="card-title text-white">{{ $product_a }}</h5>
														</div>
													</div>
												</div>
												@endif
												@if(isset($product_b_img))
												<div class="col col-12 col-sm-6 col-md-4 mt-4 mt-sm-0 ">
													<div class="card produk-item">
														<div class="card-img-top" style="background-image: url('{{ $product_b_img }}')">
														</div>
														<div class="card-body">
															<h5 class="card-title text-white">{{ $product_b }}</h5>
														</div>
													</div>
												</div>
												@endif
												@if(isset($product_c_img))
												<div class="col col-12 col-md-4 mt-4 mt-md-0 ">
													<div class="card produk-item">
														<div class="card-img-top" style="background-image: url('{{ $product_c_img }}')">
														</div>
														<div class="card-body">
															<h5 class="card-title text-white">{{ $product_c }}</h5>
														</div>
													</div>
												</div>
												@endif
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- Customer -->
						<div class="col-12 col-md-8 mx-auto">
							<div class="card w-100 customer wow fadeInUp">
								<div class="info-text text-white">
									<h3 class="info-text-title counter">{{ $customer }}</h3>
									<h5 class="info-text-desc mb-0">Customer</h5>
									<div class="city d-flex">
										<span class="h5 mb-0 info-text-desc">
											di <span class="city-count counter">{{ $city_total }}</span> Kota
										</span>
									</div>

									@if($cities)
										<div class="city-carousel carousel slide" data-ride="carousel" data-interval="2000">
											<div class="carousel-inner">
												@foreach($cities as $city)
													<div class="carousel-item @if($loop->first) active @endif">
														<span class="mb-0">{{ $city }}</span>
													</div>
												@endforeach
											</div>
										</div>
									@endif

								</div>
								<div class="info-content"></div>
							</div>
						</div>

						<!-- reseller -->
						<div class="col-12 col-md-8 mx-auto">
							<div class="card w-100 reseller wow fadeInUp">
								<div class="card-body text-center">
									<div class="info-text mb-2 text-white">
										<h3 class="h1 info-text-title counter">{{ $reseller }}</h3>
										<h5 class="info-text-desc">Total Reseller</h5>
									</div>
									
								</div>
							</div>
						</div>

						<!-- diskon -->
						<div class="col-12 col-md-8 mx-auto">
							<div class="card w-100 diskon wow fadeInUp">
								<div class="card-body">
									<div class="info-text text-white">
										<h3 class="info-text-title">Rp<span class="">{{ $discount }}</span></h3>
										<h5 class="info-text-desc">Total Diskon untuk Customer</h5>
									</div>
									<div class="info-content">

									</div>
								</div>
							</div>
						</div>

						<!-- ongkir -->
						<div class="col-12 col-md-8 mx-auto">
							<div class="card w-100 ongkir wow fadeInUp">
								<div class="card-body">
									<div class="info-text text-white">
										<h3 class="info-text-title">Rp<span class="">{{ $expedition }}</span></h3>
										<h5 class="info-text-desc">Total Ongkir</h5>
									</div>
									
								</div>
							</div>
						</div>

						<!-- omzet -->
						<div class="col-12 col-md-8 mx-auto">
							<div class="card w-100 omzet wow fadeInUp">
								<div class="card-body text-center">
									<div class="info-text mb-2 text-white">
										<h3 class="info-text-title">Rp<span class="">{{ $omzet }}</span></h3>
										<h5 class="info-text-desc">Total Omzet</h5>
									</div>
								</div>
							</div>
						</div>

						<!-- category -->
						<div class="col-12 col-md-8 mx-auto">
							<div class="card w-100 category wow fadeInUp">
								<div class="card-body">
									<div class="info-text text-white">
										<h5 class="info-text-desc mb-0">Kategori Toko Anda</h5>
										<h3 class=""><span class="">{{ $category }}</span></h3>
									</div>
									
								</div>
							</div>
						</div>

						<!-- profit -->
						<div class="col-12 col-md-8 mx-auto">
							<div class="card w-100 profit wow fadeInUp">
								<div class="card-body text-center">
									<div class="info-text mb-2 text-white">
										<h2 class="info-text-title">Rp<span class="">{{ $profit }}</span></h2>
										<h5 class="info-text-desc">Total Profit</h5>
									</div>
								</div>
							</div>
						</div>

					</div>
				</section>
			</div>
		</main>

		<!-- footer -->
		<footer class="footer pt-sm-3 pt-lg-0">
			<div class="container footer-top">
				<section class="year-text wow zoomIn">
					<div class="row">
						<div class="col-12 text-center text-white mx-auto" style="overflow: hidden">
							<div class="tilt" data-tilt data-tilt-max="4" data-tilt-speed="400" data-tilt-perspective="250">
								<img class="p-3 p-lg-5 year-logo" src="images/2020.svg" alt="" style="">
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="container footer-bottom">
				<section>
					<h5 class="share-icon-title my-3 text-white text-center">Bagikan #2019YearInReview</h5>
					<div class="d-flex justify-content-center">
						<div class="share-icon-wrap">
							<button class="share-sosmed-btn" data-sharer="facebook" data-url="{{ Request::url() }}">
								<ion-icon class="bg-fb" name="logo-facebook"></ion-icon>
							</button>
							<button class="share-sosmed-btn" data-sharer="twitter" data-url="{{ Request::url() }}">
								<ion-icon class="bg-tw" name="logo-twitter"></ion-icon>
							</button>
							<button class="share-sosmed-btn line mobile-hide" data-sharer="line" data-url="{{ Request::url() }}">
								<img class="bg-ln" src="images/icons/line2.svg" alt="">
							</button>
							<button class="share-sosmed-btn line mobile-show" data-sharer="lineMobile" data-url="{{ Request::url() }}">
								<img src="images/icons/line2.svg" alt="">
							</button>
							<button class="share-sosmed-btn" data-sharer="googleplus" data-url="{{ Request::url() }}">
								<ion-icon class="bg-gp" name="logo-googleplus"></ion-icon>
							</button>
							<button class="share-sosmed-btn mobile-hide" data-sharer="whatsapp" data-url="{{ Request::url() }}">
								<ion-icon class="bg-wa" name="logo-whatsapp"></ion-icon>
							</button>
							<button class="share-sosmed-btn mobile-show" data-sharer="whatsappMobile" data-url="{{ Request::url() }}">
								<ion-icon class="bg-wa" name="logo-whatsapp"></ion-icon>
							</button>
							<button class="share-sosmed-btn" data-sharer="telegram" data-url="{{ Request::url() }}">
								<ion-icon class="bg-tg" name="ios-paper-plane"></ion-icon>
							</button>
							<button class="share-sosmed-btn mobile-show" data-sharer="email" data-url="{{ Request::url() }}">
								<ion-icon class="bg-mail" name="mail"></ion-icon>
							</button>
							<button class="share-sosmed-btn mobile-hide" data-sharer="gmail" data-url="{{ Request::url() }}">
								<ion-icon class="bg-mail" name="mail"></ion-icon>
							</button>
							<button class="share-sosmed-btn" data-sharer="sms" data-url="{{ Request::url() }}">
								<ion-icon class="bg-sms" name="phone-portrait"></ion-icon>
							</button>
							<button class="btn-copy" data-clipboard-text="{{ Request::url() }}">
								<ion-icon class="bg-link" name="link"></ion-icon>
							</button>
						</div>
					</div>
				</section>
				<section>
					<div class="row">
						<div class="col mx-auto text-center mt-4">
							<p class="small mb-0" style="color: rgba(255,255,255,.45);">Powered by</p>
							<a href="https://ngorder.id">
								<img class="footer-logo" src="images/logo/logo-2019-white.svg" alt="Ngorder">
							</a>
						</div>
					</div>
				</section>
			</div>
		</footer>
	</div>

	<!-- script -->	
	<script>
		var chartData = <?php echo $chart; ?>;
	</script>
	<script src="https://unpkg.com/ionicons@4.2.2/dist/ionicons.js"></script>
	<script src="js/app.js"></script>

	<script>
		var thisLink=window.location.href;$('.share-sosmed-btn').attr('data-url',thisLink);$('#share-link-copy').val(thisLink);$('.btn-copy').attr('data-clipboard-text',thisLink);Chart.defaults.global.pointHitDetectionRadius=1;var ctx=$('.orderan-chart');var labelBulanPendek=["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agt","Sep","Okt","Nov","Des"];var labelBulanPanjang=["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];$(document).ready(function(){var clipboard=new ClipboardJS('.btn-copy');clipboard.on('success',function(e){alert("link copied!")});$('.counter').counterUp({delay:10,time:1000});var myChart=new Chart(ctx,{type:'bar',data:{labels:labelBulanPanjang,datasets:[{data:chartData,backgroundColor:'#4D61FC',hoverBackgroundColor:'#344bfc'}]},options:{maintainAspectRatio:!1,scales:{xAxes:[{gridLines:{drawBorder:!1,display:!1},ticks:{display:!1},}],yAxes:[{gridLines:{drawBorder:!1,display:!1},ticks:{display:!1}}]},legend:{display:!1,},tooltips:{enabled:!1,mode:'index',position:'nearest',custom:customTooltips},hover:{onHover:function(e){var point=this.getElementAtEvent(e);if(point.length)e.target.style.cursor='pointer';else e.target.style.cursor='default'}}}});$('.carousel').carousel()});var customTooltips=function(tooltip){var tooltipEl=document.getElementById('chartjs-tooltip');if(!tooltipEl){tooltipEl=document.createElement('div');tooltipEl.id='chartjs-tooltip';tooltipEl.innerHTML='<table></table>';this._chart.canvas.parentNode.appendChild(tooltipEl)}
		if(tooltip.opacity===0){tooltipEl.style.opacity=0;return}
		tooltipEl.classList.remove('above','below','no-transform');if(tooltip.yAlign){tooltipEl.classList.add(tooltip.yAlign)}else{tooltipEl.classList.add('no-transform')}
		function getBody(bodyItem){return bodyItem.lines}
		if(tooltip.body){var titleLines=tooltip.title||[];var bodyLines=tooltip.body.map(getBody);var innerHtml='<thead>';titleLines.forEach(function(title){innerHtml+='<tr><th>'+title+'</th></tr>'});innerHtml+='</thead><tbody>';bodyLines.forEach(function(body,i){var colors=tooltip.labelColors[i];var style='background:'+colors.backgroundColor;style+='; border-color:'+colors.borderColor;style+='; border-width: 2px';var span='<span class="chartjs-tooltip-key" style="'+style+'"></span>';innerHtml+='<tr><td>'+body+' orderan</td></tr>'});innerHtml+='</tbody>';var tableRoot=tooltipEl.querySelector('table');tableRoot.innerHTML=innerHtml}
		var positionY=this._chart.canvas.offsetTop;var positionX=this._chart.canvas.offsetLeft;tooltipEl.style.opacity=1;tooltipEl.style.left=positionX+tooltip.caretX+'px';tooltipEl.style.top=(positionY-60)+tooltip.caretY+'px';tooltipEl.style.fontFamily=tooltip._bodyFontFamily;tooltipEl.style.fontSize=tooltip.bodyFontSize+'px';tooltipEl.style.fontStyle=tooltip._bodyFontStyle;tooltipEl.style.padding=tooltip.yPadding+'px '+tooltip.xPadding+'px'};Chart.elements.Rectangle.prototype.draw=function(){var ctx=this._chart.ctx;var vm=this._view;var left,right,top,bottom,signX,signY,borderSkipped,radius;var borderWidth=vm.borderWidth;var cornerRadius=this._chart.config.options.cornerRadius;if(cornerRadius<0){cornerRadius=0}
		if(typeof cornerRadius=='undefined'){cornerRadius=0}
		if(!vm.horizontal){left=vm.x-vm.width/2;right=vm.x+vm.width/2;top=vm.y;bottom=vm.base;signX=1;signY=bottom>top?1:-1;borderSkipped=vm.borderSkipped||'bottom'}else{left=vm.base;right=vm.x;top=vm.y-vm.height/2;bottom=vm.y+vm.height/2;signX=right>left?1:-1;signY=1;borderSkipped=vm.borderSkipped||'left'}
		if(borderWidth){var barSize=Math.min(Math.abs(left-right),Math.abs(top-bottom));borderWidth=borderWidth>barSize?barSize:borderWidth;var halfStroke=borderWidth/2;var borderLeft=left+(borderSkipped!=='left'?halfStroke*signX:0);var borderRight=right+(borderSkipped!=='right'?-halfStroke*signX:0);var borderTop=top+(borderSkipped!=='top'?halfStroke*signY:0);var borderBottom=bottom+(borderSkipped!=='bottom'?-halfStroke*signY:0);if(borderLeft!==borderRight){top=borderTop;bottom=borderBottom}
		if(borderTop!==borderBottom){left=borderLeft;right=borderRight}}
		ctx.beginPath();ctx.fillStyle=vm.backgroundColor;ctx.strokeStyle=vm.borderColor;ctx.lineWidth=borderWidth;var corners=[[left,bottom],[left,top],[right,top],[right,bottom]];var borders=['bottom','left','top','right'];var startCorner=borders.indexOf(borderSkipped,0);if(startCorner===-1){startCorner=0}
		function cornerAt(index){return corners[(startCorner+index)%4]}
		var corner=cornerAt(0);ctx.moveTo(corner[0],corner[1]);for(var i=1;i<4;i++){corner=cornerAt(i);nextCornerId=i+1;if(nextCornerId==4){nextCornerId=0}
		nextCorner=cornerAt(nextCornerId);width=corners[2][0]-corners[1][0];height=corners[0][1]-corners[1][1];x=corners[1][0];y=corners[1][1];var radius=cornerRadius;if(radius>Math.abs(height)/2){radius=Math.floor(Math.abs(height)/2)}
		if(radius>Math.abs(width)/2){radius=Math.floor(Math.abs(width)/2)}
		if(height<0){x_tl=x;x_tr=x+width;y_tl=y+height;y_tr=y+height;x_bl=x;x_br=x+width;y_bl=y;y_br=y;ctx.moveTo(x_bl+radius,y_bl);ctx.lineTo(x_br-radius,y_br);ctx.quadraticCurveTo(x_br,y_br,x_br,y_br-radius);ctx.lineTo(x_tr,y_tr+radius);ctx.quadraticCurveTo(x_tr,y_tr,x_tr-radius,y_tr);ctx.lineTo(x_tl+radius,y_tl);ctx.quadraticCurveTo(x_tl,y_tl,x_tl,y_tl+radius);ctx.lineTo(x_bl,y_bl-radius);ctx.quadraticCurveTo(x_bl,y_bl,x_bl+radius,y_bl)}else if(width<0){x_tl=x+width;x_tr=x;y_tl=y;y_tr=y;x_bl=x+width;x_br=x;y_bl=y+height;y_br=y+height;ctx.moveTo(x_bl+radius,y_bl);ctx.lineTo(x_br-radius,y_br);ctx.quadraticCurveTo(x_br,y_br,x_br,y_br-radius);ctx.lineTo(x_tr,y_tr+radius);ctx.quadraticCurveTo(x_tr,y_tr,x_tr-radius,y_tr);ctx.lineTo(x_tl+radius,y_tl);ctx.quadraticCurveTo(x_tl,y_tl,x_tl,y_tl+radius);ctx.lineTo(x_bl,y_bl-radius);ctx.quadraticCurveTo(x_bl,y_bl,x_bl+radius,y_bl)}else{ctx.moveTo(x+radius,y);ctx.lineTo(x+width-radius,y);ctx.quadraticCurveTo(x+width,y,x+width,y+radius);ctx.lineTo(x+width,y+height);ctx.quadraticCurveTo(x+width,y+height,x+width,y+height);ctx.lineTo(x,y+height);ctx.quadraticCurveTo(x,y+height,x,y+height);ctx.lineTo(x,y+radius);ctx.quadraticCurveTo(x,y,x+radius,y)}}
		ctx.fill();if(borderWidth){ctx.stroke()}}
	</script>

	<script src="js/wow.min.js"></script>
	<script>new WOW().init();</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111582591-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-111582591-1');
	</script>
</body>
</html>