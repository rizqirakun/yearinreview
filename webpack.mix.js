const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
// 	.scripts([
// 	   	'resources/js/sosmedshare.js',
// 	   	'resources/js/jquery.counterup.js',
// 		'public/js/app.js',
// 	], 	'public/js/app.js')
// 	.extract(['jquery', 'vanilla-tilt', 'chart.js', 'moment', 'slick-carousel', 'ionicons', 'clipboard'])
// 	.autoload({
// 		jquery: ['$', 'window.jQuery', 'jQuery', 'jquery'],
// 	});



mix.scripts([
		'node_modules/jquery/dist/jquery.slim.js',
		'node_modules/vanilla-tilt/dist/vanilla-tilt.js',
		'node_modules/chart.js/dist/Chart.bundle.js',
		'node_modules/waypoints/lib/jquery.waypoints.js',
		// 'node_modules/slick-carousel/slick/slick.js',
		'node_modules/bootstrap/js/dist/util.js',
		'node_modules/bootstrap/js/dist/carousel.js',
		'node_modules/clipboard/dist/clipboard.js',
	   	'resources/js/jquery.counterup.js',
	   	'resources/js/sosmedshare.js',
	   	'resources/js/main.js',
	], 	'public/js/app.js');
	
mix.disableNotifications();
mix.sass('resources/sass/vendor.scss', 'public/css');
mix.sass('resources/sass/app.scss', 'public/css');
