<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use DB;

class ResumeController extends Controller
{
    public function index(Request $request, $shop)
    {
        // Check token
        $token_url = $request->get('t');
        try {
            $deTokenURL = Crypt::decryptString($token_url);
        } catch (DecryptException $e) {
            abort(404);
        }
        
        if(!isset($token_url) && $deTokenURL != $shop){
            abort(404);
        }

        $client = new Client(); 
        
        try{
            $result = $client->request( 'GET', 'https://staging.ngorder.id/API/year2019/'.$shop );
            // $result = $client->request( 'GET', 'http://ngorder.local:7888/API/year2018/'.$shop );

            $response = json_decode($result->getBody(), true);

            $msg = $response['message'];

            if ($msg == 'success') {
                $order         = $response['order'];
                $order_past    = $response['order_past'];

                if ($order > 0 && $order_past > 0) {
                    $order_up           = $order - $order_past;
                    $order_up_percent   = round($order_up / $order * 100);
                } else {
                    $order_up = 0;
                    $order_up_percent = 0;
                }

            
                $chart  =  array();
                foreach ($response['array_order'] as $ch) {
                    $chart[] = (int) $ch[1];
                }

                $chart = json_encode($chart);

                $product       = $response['product'];
                $product_past  = $response['product_past'];

                if ($product > 0 && $product_past > 0) {
                    $product_up           = $product - $product_past;
                    $product_up_percent   = round($product_up / $product * 100);
                } else {
                    $product_up = 0;
                    $product_up_percent = 0;
                }

                $omzet         = $this->abbre($response['omzet']);
                $discount      = $this->abbre($response['discount']);
                $expedition    = $this->abbre($response['expedition']);
                $profit        = $this->abbre($response['profit']);

                if (isset($response['array_product'][0])) {
                    $product_a     = $response['array_product'][0][0];
                    $product_a_img = $response['array_product'][0][1];
                }

                if (isset($response['array_product'][1])) {
                    $product_b     = $response['array_product'][1][0];
                    $product_b_img = $response['array_product'][1][1];
                }

                if (isset($response['array_product'][2])) {
                    $product_c     = $response['array_product'][2][0];
                    $product_c_img = $response['array_product'][2][1];
                }

                $logo       = $response['logo'];
                $customer   = $response['customer'];
                $reseller   = $response['reseller'];
                $city_total = $response['city'];
                $cities     = $response['array_city'];
                $category   = "Usaha Besar";

                if ($response['omzet'] < 300000000) $category = "Usaha Mikro";
                else if ($response['omzet'] < 2500000000) $category = "Usaha Kecil";
                else if ($response['omzet'] < 50000000000) $category = "Usaha Menengah";

                return view('year_resume', compact('shop', 'order', 'order_up', 'chart', 'order_up_percent', 'product', 'product_up', 'product_up_percent', 'product_a', 'product_a_img', 'product_b', 'product_b_img', 'product_c', 'product_c_img', 'customer', 'reseller', 'omzet', 'discount', 'expedition', 'logo', 'city_total', 'cities', 'category', 'profit'));
                
            } else {
               	abort(404);
            }
           
        } catch (GuzzleHttp\Exception\BadResponseException $e) {
            $response     = $e->getResponse();
            $responseBody = json_decode($response->getBody()->getContents(), true);

        };

    }

    function shop($id){
        $shop = DB::table('siteconfig')
            ->where('id_toko', $id)
            ->first();
        if (isset($shop)) {
            return redirect($shop->username.'?t='.Crypt::encryptString($shop->username));
        } else{
            abort(404);
        }
    }

    function abbre($rupiah){
        $abbr = array('', '', 'rb', 'jt', 'M', 'T');
        $number = explode(',', number_format($rupiah));
        $result = $number[0].(count($number)>1?','.round($number[1]/10):'').' '.$abbr[count($number)];

        return $result;
    }

    function thou($number){
        return number_format($number, 0, '', ',');
    }
}
